package com.example.hellorestapi;

import ch.qos.logback.core.util.FileUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

@RestController
public class HelloController {
    /*
    @RequestMapping("/greeting")
    public String greeting() {
        return "Hello World!";
    }
     */

    private static int id = 0;

    @RequestMapping("/greeting")
    public Greeting greeting() {
        id += 1;
        return new Greeting(id, "hello world!");
    }

    @RequestMapping(value = "/createGreeting", method = RequestMethod.POST)
    public Greeting createGreeting(@RequestBody String content) throws IOException {
       ObjectMapper mapper = new ObjectMapper();
       Greeting g = new Greeting(id,content);
       mapper.writeValue(new File("message.txt"),g);
       return g;
    }

    @RequestMapping(value = "sameGreeting", method = RequestMethod.POST)
    public Greeting sameGreeting(@RequestBody Greeting g) {
        return g;
    }

    @RequestMapping("/greetingByName")
    public String greetingByName(@RequestParam(value = "name", defaultValue = "World") String name) {
        return "Hello " + name + "!";
    }

    @RequestMapping(value = "/getHighestGreeting", method = RequestMethod.POST)
    public Greeting getHighestGreeting(@RequestBody List<Greeting> list) {
        if (list.size() == 0) throw new IllegalArgumentException("Greeting list empty");
        Greeting highest = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if (highest.getId() < list.get(i).getId())
                highest = list.get(i);
        }
        return highest;
    }

    @RequestMapping(value = "/updateGreeting", method = RequestMethod.PUT)
    public Greeting updateGreeting(@RequestBody String newMessage) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = FileUtils.readFileToString(new File("message.txt"), "UTF-8");
        Greeting g = mapper.readValue(json,Greeting.class);
        g.setContent(newMessage);
        mapper.writeValue(new File("message.txt"),g);
        return g;
    }

    @RequestMapping(value = "/deleteGreeting", method = RequestMethod.DELETE)
    public void deleteGreeting(@RequestBody int id) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = FileUtils.readFileToString(new File("message.txt"),"UTF-8");
        Greeting g = mapper.readValue(json,Greeting.class);
        if (g.getId() == id)
            FileUtils.writeStringToFile(new File("message.txt"),"","UTF-8");
    }

}
